﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour {
	
	// Contains object currently colliding with floor.
	private List<Collider> colliders = new List<Collider>();
	// Public access to blocks that are colliding with floor.
	public int collidingBlocks = 0;

	void OnCollisionEnter( Collision collision ) {
		if(!colliders.Contains(collision.collider)) { // If colliding block or object was not in list, add it.
			colliders.Add(collision.collider);
			collidingBlocks = colliders.Count;
		}
	}

	void OnCollisionExit( Collision collision ) {
		colliders.Remove(collision.collider);	// Remove object from list since it is no longer colliding.
		collidingBlocks = colliders.Count;
	}
}
