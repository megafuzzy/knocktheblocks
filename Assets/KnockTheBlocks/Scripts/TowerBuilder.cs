﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBuilder : MonoBehaviour {

	public float rotationVariance = 0.0f;
	public float locationVariance = 0.0f;
	public float cubeSizeVariance = 0.0f;

	public Block blockPrefab = null; //Primary block for instantiating new blocks for tower.
    public Block[] altBlockPrefab = { null, null, null, null, null, null }; //Alternative block prefab for other block types

    private int blockTypeIndex;
    //GameObject[] cameras = { null, null };

    float bigBlockReduceFactor = 0.40f;

    //public Material[] defaultBlockMaterials = { null, null, null, null, null };

    void Awake()
	{

        blockTypeIndex = PlayerPrefs.GetInt("BlockTypeIndex");
        //Debug.Log("Blocktype index is " + blockTypeIndex);

        if (!blockPrefab) {
			Debug.Log("BlockPrefab was empty, make sure to set it in editor.");
		}
        if (blockTypeIndex > 0 && !altBlockPrefab[blockTypeIndex - 1])
        {
            Debug.Log("One of the alternative blockPrefabs was empty, make sure to set it in editor. The empty index was "+blockTypeIndex);
        }

        //GameObject camera_A = GameObject.FindGameObjectWithTag("MainCamera");
        //GameObject camera_B = GameObject.FindGameObjectWithTag("CameraB");

        //cameras[0] = camera_A;
        //cameras[1] = camera_B;

        //if (blockTypeIndex == 0)
        //{
        //    cameras[1].GetComponent<Camera>().enabled = true;
        //    cameras[0].GetComponent<Camera>().enabled = false;
            
        //}
        //else
        //{
        //    cameras[0].GetComponent<Camera>().enabled = true;
        //    cameras[1].GetComponent<Camera>().enabled = false;
        //}

        //if (!altBlockPrefab)
        //{
        //    Debug.Log("BlockPrefab was empty, make sure to set it in editor.");
        //}



    }

    private void setSkyboxColor(int blockTypeInt)
    {
        GameObject[] walls = GameObject.FindGameObjectsWithTag("Wall");
        Color32 color;
        

        switch (blockTypeInt)
        {
            case 0:

                color = new Color32(109, 192, 102, 255); //wooden with letters
                
                
                break;
            case 1:
                color = new Color32(218, 165, 32, 255); //bronze
                break;
            case 2:
                color = new Color32(203, 190, 181, 255); //concrete
                break;
            case 3:
                color = new Color32(255, 165, 0, 255);
                break;
            case 4:
                color = new Color32(176, 224, 230, 255);
                break;
            case 5:
                color = new Color32(0, 206, 209, 255);
                break;
            case 6:
                color = new Color32(255, 195, 160, 255);
                break;
            default:
                color = new Color32(109, 192, 102, 255);
                break;
        }

        foreach (GameObject wall in walls)
        {
            //Material wallMaterial = wall.GetComponent<Renderer>().material;
            //wallMaterial.SetColor("_Tint", Color.blue);
            
            wall.GetComponent<Renderer>().material.SetColor("_Color",color);
        }
    }

    //private void useMainCamera(bool useMain)
    //{
    //    if (useMain && mainCameraInUse) //continue using main camera
    //    {
    //        return;


    //    }
    //    else if (!useMain && mainCameraInUse) //change camera from main camera to big blocks camera
    //    {
    //        Camera camA = cameras[0].GetComponent<Camera>();
    //        Camera camB = cameras[1].GetComponent<Camera>();
    //        camA.enabled = false;
    //        camB.enabled = true;
    //        mainCameraInUse = false;

    //    }
    //    else if (useMain && !mainCameraInUse) //change camera from big blocks to main camera
    //    {
    //        Camera camA = cameras[0].GetComponent<Camera>();
    //        Camera camB = cameras[1].GetComponent<Camera>();
    //        camA.enabled = true;
    //        camB.enabled = false;
    //        mainCameraInUse = true;
    //    }
    //    else return; //if for some reason big blocks camera is used twice or more in a row, so continue using it
    //}

    public void BuildTower(Vector3 buildPosition, int cubeCount)
    {
        if (blockTypeIndex > 6)
        {
            Debug.Log("Invalid block type index, should always be between 0 and 6 but is now " + blockTypeIndex);
            Debug.Log("Resetting index now to 0");
            PlayerPrefs.SetInt("BlockTypeIndex", 0);

        }

        if (!blockPrefab) {
            return;
        }
        if (blockTypeIndex > 0 && !altBlockPrefab[blockTypeIndex - 1])
        {
            return;
        }
        float tmpYposition = buildPosition.y;
        //Debug.Log("Spawned transform " + buildPosition);

        setSkyboxColor(blockTypeIndex);
        Debug.Log("TmpYPos before loop is " + tmpYposition);

        for (int i = 0; i < cubeCount; i++) {
            Block block = null;
            if (blockTypeIndex == 0)
            {
                block = Instantiate(blockPrefab);
                //cameras[1].GetComponent<Camera>().enabled = true;
                //cameras[0].GetComponent<Camera>().enabled = false;

                Vector3 temp = block.transform.localScale;

                //We change the values for this saved variable (not actual transform scale)
                temp.x = bigBlockReduceFactor;
                temp.y = bigBlockReduceFactor;
                temp.z = bigBlockReduceFactor;

                //We assign temp variable back to transform scale
                block.transform.localScale = temp;
            }
            else
            {
                block = Instantiate(altBlockPrefab[blockTypeIndex - 1]);
                //cameras[0].GetComponent<Camera>().enabled = true;
                //cameras[1].GetComponent<Camera>().enabled = false;

            }
            // Randomize size, rotation and position for each block in pile using variance values.
            //float multiplier = Random.Range(2f, 2f+cubeSizeVariance);
            //block.transform.localScale = new Vector3(multiplier, multiplier, multiplier);
            block.transform.rotation = block.transform.rotation * Quaternion.Euler(0, Random.Range(0, rotationVariance), 0);

            Vector3 tmpPos = buildPosition;
            // Set spawning position of cube
            tmpPos.y = tmpYposition;
            if (blockTypeIndex == 0)
            {
                tmpPos.y = tmpYposition - 1f;
            }

                    tmpPos.x = tmpPos.x + Random.Range(0f, locationVariance);
                    tmpPos.z = tmpPos.z + Random.Range(0f, locationVariance);
                    block.transform.position = tmpPos;
                    // Set random color for block. Colors are selected from lighter colors.
                    //Color newColor = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);

                    //Set random material for the block
                    //int cubeMaterialIndex = Random.Range(0, 4);
                    //block.GetComponent<MeshRenderer>().material = defaultBlockMaterials[cubeMaterialIndex];
                    //block.GetComponent<MeshRenderer>().material = 

                    //block.GetComponentInChildren<MeshRenderer>().material.SetColor("_Color", newColor);
                    // Set y starting point for next round.
                    //Debug.Log("Block y scale: " + block.transform.localScale.y);
                    Mesh mesh = block.GetComponent<MeshFilter>().mesh;
                    //Vector3[] vertices = mesh.vertices;
                    //Vector2[] uvs = new Vector2[vertices.Length];
                    Bounds bounds = mesh.bounds;

                    float fallHeight = bounds.size.y;
                    if (blockTypeIndex == 0)
                    {
                        //reduce the mesh bounds so that blocks do not fall from so high

                        fallHeight = fallHeight * bigBlockReduceFactor;

                    }

                    //Debug.Log("Mesh bounds is " + mesh.bounds);

                    tmpYposition += fallHeight;

                    //        if (blockTypeIndex == 0)
                    //{

                    //}             
                    //else
                    //{
                    //    tmpYposition += fallHeight; // + 0.10f;
                    //}
                    Debug.Log("TmpYPos at the end of the loop round is " + tmpYposition);
                }
            }
}

    
