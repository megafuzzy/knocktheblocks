﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {

	private Rigidbody rBody = null;

	//public float addedForce = 0; // Strength of the force when screen is pressed.

    private AudioSource audioSource;
    public AudioClip impact;
    public AudioClip tap;
    float tapVolume = 0.7f;

    void Awake() {

        audioSource = GetComponent<AudioSource>();
		rBody = GetComponent<Rigidbody>();
		if(!rBody) {
			Debug.Log("No rigidbody was found for block!");
		}
	}

    
    

	//void OnMouseOver() {
 //       Debug.Log("MouseOver");
	//	if (rBody && Input.GetMouseButtonDown (0)) { // Do raycast from camera to mouse click position and add force to that direction.
 //           Debug.Log("Block poked!");
 //           RaycastHit mouseHit; 
	//		Ray mouseRay = Camera.main.ScreenPointToRay (Input.mousePosition);
	//		Physics.Raycast (mouseRay, out mouseHit);
	//		// Add force to point in the block where ray hit.
	//		rBody.AddForceAtPosition (mouseRay.direction * addedForce, mouseHit.point);
	//	}
	//}

    private void OnCollisionEnter(Collision collision)
    {
        
        if (collision.relativeVelocity.magnitude > 5) {
            float impactVolume = collision.relativeVelocity.magnitude / 30;
            //Debug.Log("Collision magnitude " + collision.relativeVelocity.magnitude+ " and volume "+impactVolume);
            audioSource.PlayOneShot(impact, impactVolume);
        }
    }

    

    public void PlayTapSound()
    {
        audioSource.PlayOneShot(tap, tapVolume);
    }

}
