﻿using PaperPlaneTools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GoogleMobileAds.Api;

public class GameController : MonoBehaviour {

	private bool gameEnded = false; // true when all win conditions were met.
	public  int blockCount = 0; // Count of all blocks in game.

	//public Transform[] defaultSpawnPoints;	// Locations for each tower to be built. Set in editor.
    public Transform[] altSpawnPoints;

    private AudioSource gameSounds;
    public AudioClip exitbuttonClip;
 

    //private Transform[] activeSpawnPoints;

    public TowerBuilder builder;
	public Floor floor = null; // Collider of floor is used for detecting when game will end.

    //GameObject exitButton;
    //Animator exitButtonAnimator;
    
    //private float timeExitPressStarted;

    private Touch[] myTouches;
    public float addedForce = 0;

    public float zGravity = 2f;

    //Animator jooaAnimator;
    GameObject builderBoy;

    InterstitialAd interstitial;

    void Awake() {
		// Force screen orientation to landscape mode. 
		Screen.orientation = ScreenOrientation.Landscape;
        
        if (!builder) {
			Debug.Log("No TowerBuilder was found, make sure its prefab is set in Editor.");
			return;
		}

        //int blockTypeIndex = PlayerPrefs.GetInt("BlockTypeIndex");

        //if (blockTypeIndex == 0)
        //{
        //    activeSpawnPoints = defaultSpawnPoints;
            
        //}
        //else
        //{
        //    activeSpawnPoints = altSpawnPoints;
        //}

        //activeSpawnPoints = altSpawnPoints;

        foreach (Transform spawn in altSpawnPoints) { // Build tower on top of each spawnpoint, more may be added to spawnPoints list.
			int towerHeight = Random.Range (4, 7); 
			blockCount += towerHeight; // Add amount of blocks in the tower to total amount of blocks in game.
			builder.BuildTower(spawn.position, towerHeight);


        }



#if UNITY_ANDROID
        string appId = "ca-app-pub-8347667331834511~7719746888";
#elif UNITY_IPHONE
                    string appId = "ca-app-pub-3940256099942544~1458002511";
#else
                    string appId = "unexpected_platform";
#endif

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);

#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-8347667331834511/5552694642"; //my real ad unit for feed the monsters 
                                                                    //string adUnitId = "ca-app-pub-3940256099942544/1033173712"; //google sample ad unit for interstitial ads
#elif UNITY_IPHONE
                string adUnitId = "ca-app-pub-3940256099942544/4411468910";
#else
                string adUnitId = "unexpected_platform";
#endif

        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);


        Physics.gravity = new Vector3(0, zGravity, 0);

        builderBoy = GameObject.FindGameObjectWithTag("BuilderBoy");

        gameSounds = GetComponent<AudioSource>();

        builderBoy.SetActive(false);
        StartCoroutine(playStartAnimation());
    }

	void Update() {
		if(floor && floor.collidingBlocks == blockCount && !gameEnded) { // End conditions.
			gameEnded = true; // Since all blocks are colliding with floor, game has ended.
            StartCoroutine(playEndAnimation());
            
            //Debug.Log("All blocks on floor, game ended! (Not yet implemented)");
		}
        else
        {
            updateTouch();
        }

        //if (exitButtonPressed == true)
        //{
        //    //check the time which has passed from button down
        //    float timePassed = Time.time - timeExitPressStarted;
        //    if (timePassed > 0.7f)
        //    {
        //        //Debug.Log("1 seconds has passed when pressing exit button");
        //        exitGame();
        //    }
        //}      
    }


    IEnumerator playStartAnimation()
    {
        yield return new WaitForSeconds(1.5f);
        builderBoy.SetActive(true);
        Animator jooaAnimator = builderBoy.GetComponent<Animator>();
        jooaAnimator.SetTrigger("StartGame");
        yield return new WaitForSeconds(4.5f);
        builderBoy.SetActive(false);
    }

    IEnumerator playEndAnimation()
    {
        yield return new WaitForSeconds(2.5f);
        builderBoy.SetActive(true);
        Animator jooaAnimator = builderBoy.GetComponent<Animator>();
        //Debug.Log("Now trying to set trigger, animator is "+jooaAnimator+" at time: "+Time.time);
        jooaAnimator.SetTrigger("EndGame");
        yield return new WaitForSeconds(7.5f);
        //Debug.Log("Now trying to set trigger, animator is " + jooaAnimator + " at time: " + Time.time);
        builderBoy.SetActive(false);
        exitGame();
    }

    private Vector3 currentTouchPosition(Vector2 pos)
    {

        //var touchPos = Input.GetTouch(0).position; 
        //var mousePos = Input.mousePosition;
        Vector3 positionXYZ = new Vector3(pos.x, pos.y, 50.0f); // select distance = 10 units from the camera

        Vector3 inputPos;
        inputPos = Camera.main.ScreenToWorldPoint(positionXYZ);

        return inputPos;

    }

    private void updateTouch()
    {
        myTouches = Input.touches;

        foreach (Touch t in myTouches) //loop through each finger
        {
            //Debug.Log("touch detected: " + t.fingerId);
            //var inputPosition = currentTouchPosition(t.position);
            var phase = t.phase;
            //var fingerId = t.fingerId;

            switch (phase)
            {
                case TouchPhase.Began: //used only for pokes

                    //RaycastHit2D[] monsterHits = Physics2D.RaycastAll(inputPosition, inputPosition, 0.5f);
                    //showPosition(inputPosition);
                    //if (monsterHits.Length == 1) //only one object is hit
                    //{
                    //    var hit = monsterHits[0];
                    //    GameObject hitObject = hit.transform.gameObject;
                    //    if (isMonster(hitObject)) //the object which was hit was an item and the same finger is not dragging anything at the moment
                    //    {
                    //        pokeMonster(hitObject);
                    //    }
                    //}
                    //Debug.Log("Touch position is "+inputPosition);

                    //RaycastHit[] colliderHits = Physics.RaycastAll(inputPosition); //, inputPosition, 0.5f);
                    //showPosition(inputPosition);
                    //if (monsterHits.Length == 1) //only one object is hit
                    //{
                    //    var hit = monsterHits[0];
                    //    GameObject hitObject = hit.transform.gameObject;
                    //    if (isMonster(hitObject)) //the object which was hit was an item and the same finger is not dragging anything at the moment
                    //    {
                    //        pokeMonster(hitObject);
                    //    }
                    //}

                    RaycastHit hit;
                        Ray ray = Camera.main.ScreenPointToRay(t.position);
                    Debug.Log("Ray origin " + ray.origin + " and ray direction " + ray.direction);
                    
                   if (Physics.Raycast(ray, out hit, 1000f))
                        {
                        GameObject block = hit.transform.gameObject;
                        Rigidbody rBody = block.GetComponent<Rigidbody>();
                        //Debug.Log("Touch enter on " + hit.collider.name);

                        if (!rBody)
                            {
                                Debug.Log("No rigidbody was found for block!");
                            }
                        else
                            {
                            rBody.AddForceAtPosition(ray.direction * addedForce, hit.point);
                            block.SendMessage("PlayTapSound");
                            }
                        
                        }
                    
                    break;
  
            }          
        }
    }

    //public void inflateExitButton()
    //{
    //    Debug.Log("Inflate called");
    //    timeExitPressStarted = Time.time;
    //    exitButtonPressed = true;
    //    exitButtonAnimator.SetTrigger("Inflate");

    //}

    //public void deflateExitButton()
    //{
    //    Debug.Log("Deflate called");
    //    exitButtonPressed = false;
    //    exitButtonAnimator.SetTrigger("Deflate");

    //}

    public void exitGame()
    {
        //Debug.Log("Exit game called");
        MenuController.hasPlayedInThisSession = true;
        gameSounds.PlayOneShot(exitbuttonClip);

        //RateBox.Instance.Show();
        int blockType = PlayerPrefs.GetInt("BlockTypeIndex");
        blockType++;
        if (blockType == 7) blockType = 0;
        PlayerPrefs.SetInt("BlockTypeIndex", blockType);
        Debug.Log("Blocktype is now " + PlayerPrefs.GetInt("BlockTypeIndex"));

        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }

        interstitial.Destroy();

        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
		
}
