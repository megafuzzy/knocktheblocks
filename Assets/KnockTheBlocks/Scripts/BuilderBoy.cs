﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuilderBoy : MonoBehaviour {

    AudioClip[] bbClips = { null, null, null };
    private AudioSource bbSounds;

    public AudioClip endLaugh;
    public AudioClip endTalk;
    public AudioClip startTalk;

    

	// Use this for initialization
	void Start () {
        bbSounds = GetComponent<AudioSource>();
        bbClips[0] = startTalk;
        bbClips[1] = endTalk;
        bbClips[2] = endLaugh;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void playEndLaugh()
    {
        bbSounds.PlayOneShot(bbClips[2]);
        
    }

    public void playEndTalk()
    {
        bbSounds.PlayOneShot(bbClips[1]);

    }

    public void playStartTalk()
    {
        bbSounds.PlayOneShot(bbClips[0]);

    }
}
