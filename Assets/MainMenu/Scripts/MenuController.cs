﻿using UnityEngine;
//using UnityEngine.Advertisements;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using PaperPlaneTools;
//using GoogleMobileAds.Api;

public class MenuController : MonoBehaviour
{
    //public int defaultCoins = 5;
    //public int defaultRewardCoins = 5;


    //private float startingAngleForCoins = 180;

    //public RectTransform RewardCoin;
    //public RectTransform CreditCoin;
    //public RectTransform PlayCoin;

    private AudioSource menuSounds;
    //public AudioClip[] menuClips;

    //public AudioClip coinSpinAudio;
    //public AudioClip coinDropAudio;
    //public AudioClip exitButtonAudio;
    public AudioClip playButtonClip;
    //public AudioClip tvButtonAudio;

    private Image playButtonImage;
    public Sprite goldenPlayButtonSprite; //Drag your first sprite here in inspector.
    public Sprite redPlayButtonSprite; //Drag your second sprite here in inspector.
    public Sprite purplePlayButtonSprite; //Drag your second sprite here in inspector.

    public GameObject gameController;
    //private final static bool useAdMob = false;

    //public bool rewardCoinAnimationPlaying;

    //public GameObject IAPPanel;

    Button playButton;
    //Button tvButton;
    //GameObject videoMonster;

    static public bool hasPlayedInThisSession = false;

    //Button megaFuzzyButton;
    //GameObject shoppingCart;

    //private Vector3 rewardCoinDropPos;

    string appId; 
    string adUnitId;


    private void Start()
    {
        //add coins below play button
        //PlayerPrefs.DeleteKey("PlayerCoins");

        if (!PlayerPrefs.HasKey("BlockTypeIndex"))
        {
            PlayerPrefs.SetInt("BlockTypeIndex", 0);
        }

        if (!PlayerPrefs.HasKey("NoAds"))
        {
            PlayerPrefs.SetInt("NoAds", 0); //set game to free mode with ads in the beginning
        }

        //videoMonster = GameObject.FindGameObjectWithTag("VideoMonster");
        //shoppingCart = GameObject.FindGameObjectWithTag("ShoppingCart");

//#if UNITY_ANDROID
//        appId = "ca-app-pub-8347667331834511~7719746888";
//        adUnitId = "ca-app-pub-8347667331834511/5552694642";
//#elif UNITY_IPHONE
//                string appId = "ca-app-pub-3940256099942544~1458002511";
//#else
//                string appId = "unexpected_platform";
//#endif

        // Initialize the Google Mobile Ads SDK.
        //if (useAdMob) MobileAds.Initialize(appId);

        menuSounds = GetComponent<AudioSource>();
        //menuClips = new AudioClip[1]; // { headBashAudio, fistBashAudio, cheerAudio, cryAudio, excitedAudio, tvStaticAudio, tvFixingAudio, focusAudio, squeekAudio, null };
        //menuClips[0] = playButtonAudio;
        //menuClips[1] = coinDropAudio;
        //menuClips[2] = coinSpinAudio;
        //menuClips[3] = tvButtonAudio;

        //IAPPanel.SetActive(false);


        playButton = GameObject.FindGameObjectWithTag("PlayButton").GetComponent<Button>();
        playButtonImage = playButton.GetComponent<Image>();

        //tvButton = GameObject.FindGameObjectWithTag("TVButton").GetComponent<Button>();


        //megaFuzzyButton = GameObject.FindGameObjectWithTag("MegaFuzzyButton").GetComponent<Button>();


        //generateCoins();
        //if (PlayerPrefs.GetInt("PlayerCoins") < 1)
        //      {
        //          //change play button color to red?
        //          playButtonImage.sprite = redPlayButtonSprite;
        //          //redirect play button to display video ad

        //      }

        //if (PlayerPrefs.GetInt("NoAds") == 1) //1 for no ads (premium), 0 for yes ads (free)
        //{
        //    //change play button color to red?
        //    playButtonImage.sprite = goldenPlayButtonSprite;
        //    //redirect play button to display video ad
        //}

        //StartCoroutine(playRewardCoinFlyAnimation()); //testing
        //playRewardCoinFlyAnimation(); //testing
        //playRewardCoinFlyAnimation(); //testing

        if (hasPlayedInThisSession)
        {
            RateBox.Instance.Show();


        }

        var stat = RateBox.Instance.Statistics;
        //PlayerPrefs.DeleteKey("HasRated");

        //MobileAds.Initialize(appId);

        //if (stat.DialogIsRated)
        //{
        //    //Debug.Log("Player has rated the app");
        //    if (!PlayerPrefs.HasKey("HasBeenRewarded"))
        //    {
        //        //Debug.Log("Rating is not yet saved to PlayerPrefs, so this is the first time player rated");

        //        StartCoroutine(playRewardCoinFlyAnimation(5));
        //        PlayerPrefs.SetInt("HasBeenRewarded", 1);
        //    }
        //    else
        //    {
        //        //Debug.Log("..but has been given the coins already");
        //    }
        //    //gift 5 coins
        //}

        //RequestInterstitial();

    }

    //private void RequestInterstitial()
    //{


    //    // Initialize an InterstitialAd.
    //    InterstitialAd interstitial = new InterstitialAd(adUnitId);
    //    // Create an empty ad request.
    //    AdRequest request = new AdRequest.Builder().Build();
    //    // Load the interstitial with the request.
    //    interstitial.LoadAd(request);

    //    if (hasPlayedInThisSession && interstitial.IsLoaded())
    //    {
    //        interstitial.Show();

    //    }
    //}

    //private void generateCoins()
    //{


    //    if (!PlayerPrefs.HasKey("PlayerCoins")) //player has not played before or for some other reason does not have coins saved in prefs
    //    {

    //        //draw defaultCoins
    //        PlayerPrefs.SetInt("PlayerCoins", defaultCoins);
    //        //Debug.Log("Saving default number of coins "+defaultCoins + " to player prefs");
    //    }
    //    updateCoins();
    //}

    //IEnumerator freezeMenuButtons(float seconds) //can be used if internet connection is slow to prevent multiple clicks
    //{
    //    enableAllMenuButtons(false);
    //    menuSounds.PlayOneShot(menuClips[0]);
    //    yield return new WaitForSeconds(seconds);
    //    enableAllMenuButtons(true);

    //}

    //    IEnumerator playRewardCoinFlyAnimation(int rewardCoins)
    //{

    //    enableAllMenuButtons(false);
    //    //playButton.gameObject.GetComponent<Button>().enabled = false;
    //    //videoMonster.SendMessage("RandomAnimationsOnOff");

    //    Vector3 pos = videoMonster.transform.position;
    //    Debug.Log("Videomonster pos: " + pos);
    //    Quaternion rot = new Quaternion(0, 0, 45f, 0);

    //    //var timeToMove = 1f;

    //    for (int i = 0; i < rewardCoins; i++) //play animation for each coin
    //    {
    //        RectTransform coinClone = Instantiate(RewardCoin, pos, rot);
    //        coinClone.transform.SetParent(GameObject.FindGameObjectWithTag("CoinCanvas").transform);
    //        Animator coinAnimator = coinClone.GetComponent<Animator>();
    //        coinAnimator.SetTrigger("Spin");

    //        //var currentPos = coinClone.transform.position;
    //        //var t = 0f;
    //        //while (t < 1)
    //        //{
    //        //    t += Time.deltaTime / timeToMove;
    //        //    coinClone.transform.position = Vector3.Lerp(currentPos, rewardCoinDropPos, t);
    //        //    yield return null;
    //        //}


    //        Rigidbody2D rb = coinClone.GetComponent<Rigidbody2D>();
    //        float direction_y = 1f; //Random.Range(0.9f, 1.1f);
    //        float direction_x = Random.Range(-0.5f, -0.7f);
    //        Vector2 direction = new Vector2(direction_x, direction_y);
    //        float velocity = 2800f + Random.Range(-50.0f, 50.0f);
    //        rb.AddForce(direction * velocity, ForceMode2D.Impulse);
    //        menuSounds.PlayOneShot(menuClips[0]);
    //        yield return new WaitForSeconds(1.0f);
    //        videoMonster.GetComponent<Animator>().SetTrigger("Cheer"); // SendMessage("playCheerAnimation");
    //        yield return new WaitForSeconds(0.4f);
    //        //Destroy(coinClone.gameObject.GetComponent<Image>());

    //        Image im = coinClone.gameObject.GetComponent<Image>();
    //        im.enabled = false;
    //        menuSounds.PlayOneShot(menuClips[1]);
    //        yield return new WaitForSeconds(0.3f);

    //        int previousCoins = PlayerPrefs.GetInt("PlayerCoins");
    //        int updatedCoins = previousCoins + 1;
    //        //Debug.Log("Throwing coin, previously player had " + previousCoins + " coins and now he should have "+updatedCoins);
    //        PlayerPrefs.SetInt("PlayerCoins", updatedCoins);
    //        PlayerPrefs.Save();
    //        updateCoins();
    //        if (PlayerPrefs.GetInt("PlayerCoins") > 0)
    //        {
    //            playButtonImage.sprite = purplePlayButtonSprite;
    //        }
    //        ////Debug.Log("Now should wait for 2 more secs, at time: "+Time.time);
    //        yield return new WaitForSeconds(0.2f);
    //        ////Debug.Log("Now wait is over, time: " + Time.time);
    //    }

    //    //playButton.gameObject.GetComponent<Button>().enabled = true;
    //    videoMonster.SendMessage("RandomAnimationsOnOff");             
    //    enableAllMenuButtons(true);
    //}

    //  private void enableAllMenuButtons(bool enabled)
    //  {
    //      //Debug.Log("Setting menu buttons to " + enabled);



    //      //if (!enabled)
    //      //{
    //      //    Color tmp = playButton.GetComponent<SpriteRenderer>().color;
    //      //    tmp.a = 0f;
    //      //    playButton.gameObject.GetComponent<Button>.
    //      //}
    //      //else
    //      //{
    //      //    Color tmp = playButton.GetComponent<SpriteRenderer>().color;
    //      //    tmp.a = 255f;
    //      //    playButton.GetComponent<SpriteRenderer>().color = tmp;
    //      //}

    //      //if (!enabled)
    //      //{
    //      //    playButton.gameObject.GetComponent<Button>().enabled = enabled;
    //      //    Color buttonColor = playButtonImage.color;
    //      //    buttonColor.r = 0f;
    //      //    //Debug.Log("Button color is " + buttonColor.r);
    //      //}
    //      //tvButton.gameObject.GetComponent<Button>().interactable = enabled;
    //      playButton.gameObject.GetComponent<Button>().interactable = enabled;
    //megaFuzzyButton.gameObject.GetComponent<Button>().interactable = enabled;
    //      //playButton.SetActive(enabled);
    //      //tvButton.SetActive(enabled);

    //  }

   
    public void launchGame()
    {


        //videoMonster.SendMessage("RandomAnimationsOnOff");
        //if (PlayerPrefs.GetInt("PlayerCoins") > 0)
        //{
        //    StartCoroutine(expendCoinAndClearMenuScreen()); //always 1
        //    int coins = PlayerPrefs.GetInt("PlayerCoins");
        //    coins--;
        //    PlayerPrefs.SetInt("PlayerCoins", coins);
        //    PlayerPrefs.Save();
        //    Debug.Log("Player has " + coins + " coins");
        //}
        //else
        //{
        //    ShowRewardedAd();
        //}
        //menupanel alpha
        //videoMonster.SendMessage("RandomAnimationsOnOff");
        menuSounds.PlayOneShot(playButtonClip);
        Debug.Log("Now launching scene 1");
        SceneManager.LoadScene(1, LoadSceneMode.Single);

    }

    //IEnumerator expendCoinAndClearMenuScreen()
    //{
    //    enableAllMenuButtons(false);

    //    //videoMonster.SendMessage("RandomAnimationsOnOff");
    //    GameObject expendableCoin = GameObject.FindGameObjectWithTag("CreditCoin"); //get one of the coins, does not matter which
    //    Quaternion rot = new Quaternion(0, 0, 0, 0);

    //    RectTransform coinClone = Instantiate(PlayCoin, expendableCoin.transform.position, rot);
    //    //coinClone.transform.SetParent(GameObject.FindGameObjectWithTag("MenuPanel").transform);
    //    //Debug.Log("Coin sibling index; "+coinClone.GetSiblingIndex());
    //    coinClone.transform.SetParent(GameObject.FindGameObjectWithTag("CoinCanvas").transform);
    //    //Debug.Log("Coin sibling index; " + coinClone.GetSiblingIndex());
    //    Rigidbody2D rb = coinClone.GetComponent<Rigidbody2D>();
    //    rb.gravityScale = 0;
    //    Animator coinAnimator = coinClone.GetComponent<Animator>();
    //    //coinClone.tag = "PlayCoin"; //so that other processes do not destroy this coin with tag CreditCoin
    //    coinAnimator.SetTrigger("Play");
    //    menuSounds.PlayOneShot(menuClips[0]);
    //    GameObject.Destroy(expendableCoin);
    //    //Image eImage = expendableCoin.GetComponent<Image>();
    //    //eImage.enabled = false;
    //    yield return new WaitForSeconds(2.75f);
    //    Debug.Log("Now launching scene 1");
    //    SceneManager.LoadScene(1, LoadSceneMode.Single);


    //    //can everything after this be removed?

    //    //Image im = coinClone.gameObject.GetComponent<Image>();
    //    //im.enabled = false;
    //    //clearCreditCoins();
    //    //GameObject canvas = GameObject.FindGameObjectWithTag("Canvas");
    //    //canvas.SetActive(false);


    //}



    //private void clearCreditCoins()
    //{
    //    GameObject[] deletableCoins = GameObject.FindGameObjectsWithTag("CreditCoin");
    //    foreach (GameObject cloneCoin in deletableCoins)
    //    {
    //        //Debug.Log("Deleting coin");
    //        GameObject.Destroy(cloneCoin);
    //    }
    //}

    //private void updateCoins()
    //{

    //    //int previousCoins = PlayerPrefs.GetInt("PlayerCoins");
    //    //int updatedCoins = previousCoins + rewardCoins;
    //    //PlayerPrefs.SetInt("PlayerCoins", updatedCoins);
    //    clearCreditCoins();
    //    int coins = PlayerPrefs.GetInt("PlayerCoins");
    //    //Debug.Log("updateCoins: Player has " + coins + " coins ");
    //    GameObject pButtonGO = GameObject.FindGameObjectWithTag("PlayButton");

    //    Vector3 center = pButtonGO.transform.position;
    //    Debug.Log("Playbutton position: " + center);
    //    double coinRad = 0;
    //    Debug.Log("Screen height is " + Screen.height);
    //    coinRad = Screen.height * 0.17; //radius for coins from the center should be 17% of the screen height so that they are positioned correctly
    //    float radFloat = 0;

    //    for (int i = 0; i < coins; i++)
    //    { 
    //        ////Debug.Log("Adding coin "+i);


    //        radFloat = (float)coinRad;
    //        Vector3 pos = circlePos(center, radFloat, i, coins);
    //        //if (i==0)
    //        //{
    //        //    rewardCoinDropPos = pos;
    //        //}
    //        Quaternion rot = new Quaternion(0, 0, 0, 0); //Quaternion.FromToRotation(Vector3.forward, center - pos);
    //        RectTransform coinClone = Instantiate(CreditCoin, pos, rot);
    //        Rigidbody2D rb = coinClone.GetComponent<Rigidbody2D>();
    //        rb.gravityScale = 0;
    //        coinClone.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform);
    //        Animator coinAnimator = coinClone.GetComponent<Animator>();
    //        //yield return new WaitForSeconds(Random.Range(0f,1f));
    //        coinAnimator.SetTrigger("Glimmer");
    //        //coinClone.tag = "CreditCoin";
    //    }
    //}

    //Vector3 circlePos(Vector3 center, float radius, int coinIndex, int numberOfCoins)
    //{

    //    float ang;
    //    coinIndex++; //as we cannot use zero, zeroindex should be at 180 degrees.

    //    float angleDelta = 20f;

    //    startingAngleForCoins = 170 - numberOfCoins * 10;

    //    //if (numberOfCoins == 1) startingAngleForCoins = 160;
    //    //if (numberOfCoins == 2) startingAngleForCoins = 150;
    //    //if (numberOfCoins == 3) startingAngleForCoins = 140;
    //    //if (numberOfCoins == 4) startingAngleForCoins = 130;
    //    //if (numberOfCoins == 5) startingAngleForCoins = 120;
    //    //if (numberOfCoins == 6) startingAngleForCoins = 110;
    //    //if (numberOfCoins == 7) startingAngleForCoins = 100;
    //    //if (numberOfCoins == 8) startingAngleForCoins = 90;
    //    if (numberOfCoins >= 9) {
    //        startingAngleForCoins = 80;
    //        float numberOfCoinsFloat = (float)numberOfCoins;
    //        angleDelta = 180 / numberOfCoinsFloat;
    //        //angleDelta = angleDelta * 2;
    //    }

    //    ang = startingAngleForCoins + (angleDelta * coinIndex);


    //    //Debug.Log("Adding coin with index " + coinIndex + " and angle " + ang + " while angleDelta was " + angleDelta);

    //    Vector3 pos;
    //    pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
    //    pos.y = center.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
    //    pos.z = center.z;
    //    return pos;
    //}



    //public void ShowRewardedAd()
    //{
    //    //Debug.Log("Show rewarded Ad");
    //    //remove this when putting ads live
    //    //int previousCoins = PlayerPrefs.GetInt("PlayerCoins");
    //    //int updatedCoins = previousCoins + rewardCoins;
    //    //PlayerPrefs.SetInt("PlayerCoins", updatedCoins);
    //    //playRewardCoinFlyAnimation();
    //    menuSounds.PlayOneShot(menuClips[3]);
    //    freezeMenuButtons(0.5f); //implement slight delay before ad is displayed
    //    if (Advertisement.IsReady("rewardedVideo"))
    //    {
    //        var options = new ShowOptions { resultCallback = HandleShowResult };
    //        Advertisement.Show("rewardedVideo", options);
    //    }
    //}

    //private void HandleShowResult(ShowResult result)
    //{
    //    switch (result)
    //    {
    //        case ShowResult.Finished:
    //            //Debug.Log("The ad was successfully shown.");
    //            StartCoroutine(playRewardCoinFlyAnimation(defaultRewardCoins));
    //            //
    //            // YOUR CODE TO REWARD THE GAMER
    //            // Give coins etc.
    //            break;
    //        case ShowResult.Skipped:
    //            //Debug.Log("The ad was skipped before reaching the end.");
    //            break;
    //        case ShowResult.Failed:
    //            //Debug.LogError("The ad failed to be shown.");
    //            break;
    //    }
    //}

    //public void loadMenuScene()
    //{
    //    Debug.Log("Load menu scene called");

    //    SceneManager.LoadScene(0);
    //}

    public void openMarketPage()
    {
#if UNITY_IOS
        Application.OpenURL("http://itunes.com/apps/megafuzzy");
#endif

#if UNITY_Android
        Application.OpenURL("market://dev?id=03379144440029920632");
#endif

#if UNITY_EDITOR
        Application.OpenURL("https://www.megafuzzy.com");
#endif
    }


    public void openRateDialog()
    {

    }

    public void openAppPage()
    {
#if UNITY_IOS
        Application.OpenURL("http://itunes.com/apps/megafuzzy");
#endif

#if UNITY_Android

        Application.OpenURL("market://details?id=com.megafuzzy.knocktheblocks");
       
        
#endif

#if UNITY_EDITOR
        Application.OpenURL("https://www.megafuzzy.com");
#endif
    }
}

