# Project description

Knock The Blocks is an amazingly fun and lovable game by MegaFuzzy, aimed at babies 6 months and up, toddlers and even preschoolers. Instead of building block towers this game lets your little one break them one after another which is obviously much more fun. When all the towers are down your child will surely be rewarded with a laughter every time, guaranteed!

* Authentic toddler sounds 
* Multitouch supported for playing together with a parent or a sibling

**Please note, this project has not been migrated to 64-bit and therefore is deprecated and not available in Play store anymore. This repository is here only for the job hunting purposes**

# My role in development and technologies used

* The game was developed solely by myself, using Unity3D and scripting with C#. 
* All credits for character graphics go to https://carolinamorera.com/
* The toddler vocals recorded from my own son when he was 3 years old :)
